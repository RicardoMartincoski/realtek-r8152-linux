#
#
#

CONFIG_CTAP_SHORT = ON

ifneq ($(KERNELRELEASE),)
	obj-m	 := r8152.o
#	EXTRA_CFLAGS += -DRTL8152_S5_WOL
#	EXTRA_CFLAGS += -DRTL8152_DEBUG
	ifneq (,$(filter OFF off, $(CONFIG_CTAP_SHORT)))
		EXTRA_CFLAGS += -DCONFIG_CTAP_SHORT_OFF
	endif

	ifneq ($(BUILD_TEST_DRIVER),)
		obj-m += r8152-test.o
	endif

	TEST_SRC_DIR=.utootlkm/package/realtek-r8152-linux/src
.PHONY: copy_files
copy_files:
	rm -rf $(TEST_SRC_DIR)/
	mkdir -p $(TEST_SRC_DIR)
	cp -f * $(TEST_SRC_DIR)/

.PHONY: test
test: copy_files
	.utootlkm/docker-run make V=1 test-inside-docker

.PHONY: test-debug
test-debug: copy_files
	.utootlkm/docker-run make V=2 test-inside-docker

else
	KERNELDIR ?= /lib/modules/$(shell uname -r)/build
	PWD :=$(shell pwd)
	TARGET_PATH := kernel/drivers/net/usb
	INBOXDRIVER := $(shell find $(subst build,$(TARGET_PATH),$(KERNELDIR)) -name r8152.ko.* -type f)
	RULEFILE = 50-usb-realtek-net.rules
	RULEDIR = /etc/udev/rules.d/

.PHONY: modules
modules:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

.PHONY: all
all: clean modules install

.PHONY: clean
clean:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) clean

.PHONY: install
install:
ifneq ($(shell lsmod | grep r8152),)
	rmmod r8152
endif
ifneq ($(INBOXDRIVER),)
	rm -f $(INBOXDRIVER)
endif
	$(MAKE) -C $(KERNELDIR) M=$(PWD) INSTALL_MOD_DIR=$(TARGET_PATH) modules_install
	modprobe r8152

.PHONY: install_rules
install_rules:
	install --group=root --owner=root --mode=0644 $(RULEFILE) $(RULEDIR)

endif

